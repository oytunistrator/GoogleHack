package ocorp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class GoogleHack{
    public String text;
    public String type;

    public GoogleHack(String text, String type) {
        this.text = text;
        this.type = type;
    }

    public String connect(){
        System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
        java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider()); 

        BufferedReader reader = null;
        StringBuilder stringBuilder;

        try{
            URL url1 = new URL("http://www.google.com");
            HttpURLConnection con =  (HttpURLConnection) url1.openConnection();
            con.setRequestMethod("GET");
            con.setReadTimeout(15*1000);
            con.connect();

            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null){
                stringBuilder.append(line + "\n");
            }

            return stringBuilder.toString();

        }
        catch(Exception e){
            System.out.println("Something bad just happened."); 
            System.out.println(e);  
        }
        finally{
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException ioe) {
                    System.out.println(ioe);  
                }
            }
        }
        return "Err!";
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        GoogleHack hack = new GoogleHack("test","test");
        System.out.println(hack.connect());
    }
}
